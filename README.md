# Ansible Prometheus Junos Exporter

Example config:
```
junos_exporter__config:
  devices: "[
             {% for item in groups['router_junos'] | d({}) %}
               {
                 'host': '{{ item }}',
                 'username': 'monitoring',
                 'key_file': '/var/lib/junos_exporter/.ssh/monitoring',
               },
             {% endfor %}
            ]
           "
  features:
    alarm: true
    bgp: true
    environment: true
    interfaces: true
    interface_diagnostic: true
    interface_queue: true
    ipsec: false
    ospf: false
    isis: true
    ldp: false
    routes: true
    firewall: true
    routing_engine: true
    storage: true
    accounting: false
    fpc: true
    l2circuit: false
    rpki: true
    satellite: false
```

vault:
```
junos_exporter__keys:
  - key: |
      -----BEGIN OPENSSH PRIVATE KEY-----
      ....
      -----END OPENSSH PRIVATE KEY-----
    name: "monitoring"
~
```
